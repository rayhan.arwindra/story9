from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

class testAjaxJson(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()

        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        self.browser.get('http://localhost:8000')

    def tearDown(self):
        self.browser.close()

    def test_page_is_displayed(self):
        self.assertIn('Story9', self.browser.title)
        assert "Welcome! Enter your favourite book series:"

    def test_search_bar(self):
        search_bar = self.browser.find_element_by_name("searchBar")
        search_btn = self.browser.find_element_by_name("searchBtn")

        search_bar.send_keys("Andrzej Sapkowski")
        search_btn.send_keys(Keys.RETURN)

        time.sleep(5)

        assert "The Last Wish" in self.browser.page_source
    
    def test_like_button(self):
        search_bar = self.browser.find_element_by_name("searchBar")
        search_btn = self.browser.find_element_by_name("searchBtn")

        search_bar.send_keys("Andrzej Sapkowski")
        search_btn.send_keys(Keys.RETURN)

        time.sleep(5)

        like_btn = self.browser.find_element_by_name("likeBtn")
        likes = self.browser.find_element_by_name("likes")
        
        assert "0" in likes.text

        like_btn.send_keys(Keys.RETURN)

        assert "1" in likes.text;


    def test_modal_shows_most_liked_books(self):
        search_bar = self.browser.find_element_by_name("searchBar")
        search_btn = self.browser.find_element_by_name("searchBtn")

        search_bar.send_keys("Andrzej Sapkowski")
        search_btn.send_keys(Keys.RETURN)

        time.sleep(5)

        like_btn = self.browser.find_element_by_name("likeBtn")
        likes = self.browser.find_element_by_name("likes")

        like_btn.send_keys(Keys.RETURN)

        time.sleep(1)

        modal_btn = self.browser.find_element_by_name("modalBtn")
        modal_btn.send_keys(Keys.RETURN)

        modal_body = self.browser.find_element_by_class_name("modal-body")
        
        time.sleep(3)

        assert "The Last Wish" in modal_body.text
# Create your tests here.

