initJS();

function initJS(){
    
    var loadedBooks = [];
    var booksSortedByLikes = [];

    $("#searchBtn").click(function(){
        init();
    });

    $("input").on('keypress', function (e){
        if (e.which === 13){
            init();
        }
    });

    function init(){
        $("table").removeClass("d-none");
        search();
    }

    function search(){
        var searchVal = $("input").val();
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q="+searchVal,
            method:"GET"
        }).done(function(data){
            var bookData = data.items;
            displayBooks(bookData);
        })
        .fail(function (){
            alert("No results found");
        });
    }

    function displayBooks(books){
        $.each(books, function(index, value){
            var htmlString = renderHtml(index,value);
            $("#searchResult tr:last").after(htmlString);
        });
    }

    function renderHtml(index, value){
        var volume = value.volumeInfo;
        var index = addObject(volume);

        if (typeof index != 'undefined'){
            var numberCol = index;
            var currentBook = loadedBooks[index - 1];

            var imageCol = "<img src=" + currentBook.image + ">";
            var titleCol = currentBook.title;
            var authorCol = currentBook.author;
            var likeCol = currentBook.likes;
            return `<tr> 
                        <td>` + numberCol +`</td>
                        <td>` + imageCol + `</td>
                        <td>` + titleCol + `</td>
                        <td>` + authorCol +`</td>
                        <td> 
                        <div name="likes" id="likes">` 
                        + likeCol +  
                        `</div>
                        <button name="likeBtn" class="btn btn-primary likeBtn">Like <i class="far fa-thumbs-up"></i></button>
                        </td>
                    </tr>`
        }
        
    }

    function addObject(bookVolume){
        var newBook = {
            "title" : bookVolume.title,
            "image" : bookVolume.imageLinks.thumbnail,
            "author": (typeof bookVolume.authors === 'undefined') ? "None" : bookVolume.authors[0],
            "likes" : 0
        }

        if (!isDuplicate(newBook)){

            loadedBooks.push(newBook);
            booksSortedByLikes.push(newBook);
            return loadedBooks.length;

        }
        return undefined;
    }

    function isDuplicate(book){
        var found = false;
        $.each(loadedBooks, function(index,value){
            if (equivalent(book,value)){
                found = true;
                return false;
            }
        });
        return found;
    }

    function equivalent (newBook, book){
        return book.title === newBook.title
            && book.author === newBook.author;
    }

    $("table").on("click",".likeBtn", function(event){
        var likedBookIdx = ($(this).closest("tr").index() - 1);
        var idxLike = ++loadedBooks[likedBookIdx].likes;
        $(this).siblings().text(idxLike);

        sortBooks();
    });

    function sortBooks(){
        booksSortedByLikes.sort(function(a,b){
            return b.likes - a.likes;
        });

        updateModal();
    }

    function updateModal(){
        var fiveTopBooks = getFiveTopBooks();
        var modalHTML = (renderModalHtml(fiveTopBooks));
        var html = $.parseHTML(modalHTML);
        $(".modal-body").html(html);
    }

    function getFiveTopBooks(){
        var fiveTopBooks = booksSortedByLikes.slice(0,5);
        var htmlArr = [];
        $.each(fiveTopBooks, function(index,value){
            var idxCol = index + 1;
            var titleCol = value.title;
            var likeCol = value.likes;
            htmlArr.push(`<tr>
                            <td>` + idxCol + `</td>
                            <td>` + titleCol + `</td>
                            <td>` + likeCol + `</td>
                        <tr>`);
        });
        return htmlArr;
    }

    function renderModalHtml(fiveTopBooks){
        var htmlString = fiveTopBooks.join(" ");
        return `<table class="table table-hover">
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Likes</th>
                        </tr>`+
                    htmlString +
                    `</table>`;

    }
}
